/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.metro.train.ticket.booking.system.service.impl;

import in.ac.gpckasaragod.metro.train.ticket.booking.system.service.TravellerDetailsService;
import in.ac.gpckasaragod.metro.train.ticket.booking.system.ui.data.TravellerDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TravellerDetailsServiceImpl extends ConnectionServiceImpl implements TravellerDetailsService{

    /**
     *
     * @param travellerName
     * @param age
     * @param gender
     * @param phone
     * @param id
     * @param trainId
     * @param ticketNo
     * @param totalPrice
     * @return
     */
    @Override
    public String saveTravellerDetails(String travellerName,Integer age,String gender,String phone,Integer id,Integer trainId,Integer ticketNo,Double totalPrice){
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "','"+"INSERT INTO TravellerDetails(TRAVELLERNAME,AGE,PHONE,GENDER,PHONE,ID,TRAINID,TICKETNO,TOTALPRICE,DATE) Values '+'('"+travellerName+"','"+age+"','"+gender+"','"+phone+"','"+id+"','"+trainId+"','"+ticketNo+"','"+totalPrice;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "saved succesfully";
            }
           
    
           } catch (SQLException ex) {
            Logger.getLogger(TravellerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
          return "Save failed";
    }
    

    @Override
    public List<TravellerDetail> getAllTravellerDetails() {
        List <TravellerDetails> travellerDetails = new ArrayList<>();
        try{
            Connection connection =  getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM TRAVELLER_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
               while(resultSet.next()){
        String travellerName = resultSet.getString("TRAVELLER_NAME");
        Integer age = resultSet.getInt("AGE");
        String gender = resultSet.getString("GENDER");
        String phone = resultSet.getString("PHONE");
        Integer id = resultSet.getInt("ID");
        Integer trainId = resultSet.getInt("TRAIN_ID");
        Integer ticketNo = resultSet.getInt("TICKET_NO");
        Double totalPrice = resultSet.getDouble("TOTAL_PRICE ");        
        TravellerDetail travellerdetail = new TravellerDetail(travellerName,age,gender,phone,id,trainId,ticketNo,totalPrice);
        travellerdetail.add(travellerdetail);
        }
    }   catch (SQLException ex) {
            Logger.getLogger(TravellerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
       }
        List<TravellerDetail> TravellerDetail = null;
         return TravellerDetail;
        }
    


    @Override
    public String deleteTravellerDetails(Integer id){
     try{
         Connection connection =getConnection();
         String query = "DELETE FRPM TRAVELLER_DETAILS WHERE ID =?";
         PreparedStatement statement =connection.prepareStatement(query);
         statement.setInt(1,id);
         int delete =statement.executeUpdate();
         if(delete !=1)
             return "Delete failed";
         else
             return "Deleted successfully";
     } catch (SQLException ex) {
         Logger.getLogger(TravellerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
     return "Delete failed";
    }

    @Override
    public TravellerDetail readTravellerDetails(Integer id) {
        TravellerDetail travellerDetail = null;
    try{
       Connection connection = getConnection();
       Statement statement = connection.createStatement();
       String query = "SELECT * FROM TRAVELLER_DETAILS WHERE ID="+id;
       ResultSet resultset = statement.executeQuery(query);
       
       while (resultset.next()){
        String travellerName = resultset.getString("TRAVELLER_NAME");
        Integer age = resultset.getInt("AGE");
        String gender = resultset.getString("GENDER");
        String phone = resultset.getString("PHONE");
        Integer trainId = resultset.getInt("TRAIN_ID");
        Integer ticketNo = resultset.getInt("TICKET_NO");
        Double totalPrice = resultset.getDouble("TOTAL_PRICE ");
           TravellerDetails travellerDetails = new TravellerDetails(travellerName,age,gender,phone,id,trainId,ticketNo,totalPrice);
       }
       return travellerDetail;
    }catch (SQLException ex) {
      Logger.getLogger(TravellerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
      return travellerDetail;

    }


    @Override
    public String updateTravellerDetails(String travellerName, Integer age, String gender, String phone, Integer id, Integer trainId, Integer ticketNo, Double totalPrice) {
      try{ 
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE TRAVELLER_DETAILS SET TRAVELLER_NAME='"+ travellerName +"',AGE='" +age+ "',GENDER='"+gender+"',PHONE='" +phone+ "',ID='" +id+ "',TRAIN_ID='" +trainId+ "',TICKET_NO='" +ticketNo+ "',TOTAL_PRICE='" +totalPrice+ "' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update!=1)
                return "Updated failed";
            else
                return "Updated successfully";
        } catch (SQLException ex) {
            Logger.getLogger(TravellerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Update failed";
        }      
    }
}