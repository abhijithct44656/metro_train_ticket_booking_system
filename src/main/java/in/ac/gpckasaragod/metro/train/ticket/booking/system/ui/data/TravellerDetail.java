/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.metro.train.ticket.booking.system.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TravellerDetail {
    private String travelerName;
    private Integer age;
    private String gender;
    private String phone;
    private Integer id;
    private Integer trainId;
    private Integer ticketNo;
    private Double totalPrice;

    public TravellerDetail(String travelerName, Integer age, String gender, String phone, Integer id, Integer trainId, Integer ticketNo, Double totalPrice) {
        this.travelerName = travelerName;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.id = id;
        this.trainId = trainId;
        this.ticketNo = ticketNo;
        this.totalPrice = totalPrice;
    }
    private static final Logger LOG = Logger.getLogger(TravellerDetail.class.getName());
    

    public String getTravelerName() {
        return travelerName;
    }

    public void setTravelerName(String travelerName) {
        this.travelerName = travelerName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTrainId() {
        return trainId;
    }

    public void setTrainId(Integer trainId) {
        this.trainId = trainId;
    }

    public Integer getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(Integer ticketNo) {
        this.ticketNo = ticketNo;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void add(TravellerDetail travellerdetail) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
}