/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.metro.train.ticket.booking.system.service;

import in.ac.gpckasaragod.metro.train.ticket.booking.system.ui.data.TravellerDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface TravellerDetailsService {
     public String saveTravellerDetails(String travellerName,Integer age,String gender,String phone,Integer id,Integer trainId,Integer ticketNo,Double totalPrice);
    public TravellerDetail readTravellerDetails(Integer id);
    public List<TravellerDetail>getAllTravellerDetails();
    
    public String updateTravellerDetails(String travellerName,Integer age,String gender,String phone,Integer id,Integer trainId,Integer ticketNo,Double totalPrice);
    public String deleteTravellerDetails(Integer id);
}
