/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.metro.train.ticket.booking.system.service;

/**
 *
 * @author student
 */
public interface MetroTrainTicketBookingService {
    public String saveMetroTrainTicketBookingSystem(String trainName,Integer id,String startPlace,String destination,String classType,Double price);
}
